﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour
{
    [System.Serializable]
    public class Wave
    {
        public GameObject enemyPrefab;
        public string waveName;
        public int waveSize;
    }
    public float timeBetweenWaves = 8f;
    private float countdown = 4f;

    public Wave[] wave;

    private int waveNumber = 0;

    void Update()
    {
            if (countdown <= 0f)
            {
                SpawnWave(wave[waveNumber]);
                countdown = timeBetweenWaves;
            }

            countdown -= Time.deltaTime;
    }

    void SpawnWave(Wave _wave)
    {
        for (int i = 0; i < _wave.waveSize; i++)
        {
            SpawnEnemy(_wave.enemyPrefab);
        }

        waveNumber++;

    }

    void SpawnEnemy(GameObject enemy)
    {
        Instantiate(enemy, transform.position, Quaternion.identity);
    }

    bool EnemyChecker()
    {
        if (GameObject.FindGameObjectsWithTag("ground type") == null || GameObject.FindGameObjectsWithTag("flying type") == null)
        {
            return false;
        }
        else
        {
            return true;
        }

    }
}
