﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

    public GameObject targetPoint;
    NavMeshAgent agent;

	void Start () {

        agent = GetComponent<NavMeshAgent>();
        targetPoint = GameObject.FindGameObjectWithTag("Target Point");

    }
	
	void Update () {

        agent.SetDestination(targetPoint.transform.position);

        if (Vector3.Distance(transform.position, targetPoint.transform.position) < 1)
        {
            Destroy(gameObject);
        }
	}
}
