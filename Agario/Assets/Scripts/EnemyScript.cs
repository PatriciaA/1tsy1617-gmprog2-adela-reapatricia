﻿using UnityEngine;
using System.Collections;

public enum EnemyState
{
    Patrol,
    Chase,
    Eat,
    Run,
    Dead,
}

public class EnemyScript : MonoBehaviour
{

    public EnemyState enemyState = EnemyState.Patrol;
    private Transform target;
    private float speed = 5;
    private Vector3 randomTarget;

    void Update()
    {
        switch (enemyState)
        {
            case EnemyState.Patrol:
                Patrol();
                break;
            case EnemyState.Chase:
                Chase();
                break;
            case EnemyState.Run:
                Run();
                break;
            case EnemyState.Dead:
                Dead();
                break;
        }

        if (GetComponentInChildren<EnemyDetection>().otherEnemy != null)
            target = GetComponentInChildren<EnemyDetection>().otherEnemy.transform;
    }


    void Patrol()
    {
        Debug.Log("Patrol");
        Debug.Log(Vector3.Distance(this.transform.position, randomTarget));
        if (Vector3.Distance(this.transform.position, randomTarget) < 2)
        {
            float posX = Random.Range(-20, 20);
            float posY = Random.Range(-20, 20);
            randomTarget = new Vector3(posX, posY, 0);
        }

        this.transform.position =
            Vector3.MoveTowards(this.transform.position, randomTarget, Time.deltaTime * speed);
        // Random Movement... X, Y
        // change state to chase when u see a player, AI smaller than you
        // change state to Eat when u see a Food
        // change state to Run when u see a  player, Ai bigger than you

        if (target)
        {
            enemyState = EnemyState.Chase;
        }
    }

    void Chase()
    {
        Debug.Log("Chase");

        if (Vector3.Distance(this.transform.position, target.transform.position) > 5)
        {
            target = null;
            enemyState = EnemyState.Patrol;
        }
        else
        {
            this.transform.position =
            Vector3.MoveTowards(this.transform.position, target.transform.position, Time.deltaTime * speed);
        }
        // Move Toward Other Living Object in-game
    }
    

    void Run()
    {
        // Run Away to the Detected enemy bigger than you
        if(transform.localScale.x < target.localScale.x)
        {
            transform.position = Vector3.MoveTowards(transform.position, -target.position, speed * Time.deltaTime);
        }
    }

    void Dead()
    {
        //Dead
    }
}
