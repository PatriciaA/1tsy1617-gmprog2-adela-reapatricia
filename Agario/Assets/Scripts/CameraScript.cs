﻿using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {

    public GameObject player;
    public float CameraSpeed = 5f;
    private Vector3 PlayerPosition;
    
	void Update ()
    {
        PlayerPosition = player.transform.position;
        PlayerPosition.z = -10;

        transform.position = Vector3.MoveTowards(transform.position, PlayerPosition, CameraSpeed * Time.deltaTime / transform.localScale.x);
	}
}
