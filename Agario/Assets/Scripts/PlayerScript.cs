﻿using UnityEngine;
using System.Collections;

public class PlayerScript : MonoBehaviour {

    public float Speed;
    public Vector3 MousePositioning;

    void Update ()
    {
        MousePositioning = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        MousePositioning.z = 0;
        transform.position = Vector3.MoveTowards(new Vector3(Mathf.Clamp(transform.position.x, -9.0f, 9.0f), Mathf.Clamp(transform.position.y, -9.0f, 9.0f), 0), MousePositioning, Speed * Time.deltaTime / transform.localScale.x);
	}
}
