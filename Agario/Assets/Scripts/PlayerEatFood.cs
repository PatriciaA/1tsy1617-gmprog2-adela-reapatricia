﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerEatFood : MonoBehaviour {

    public float IncScale;
    public Text Letters;

    int Score = 0;
    
	void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        transform.localScale += new Vector3(IncScale, IncScale, IncScale);

        Score += 5;
        Letters.text = "Score: " + Score;
    }
}
