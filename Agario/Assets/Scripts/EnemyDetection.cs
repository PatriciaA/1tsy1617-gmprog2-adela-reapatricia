﻿using UnityEngine;
using System.Collections;

public class EnemyDetection : MonoBehaviour
{
    public GameObject otherEnemy;

    void OnTriggerEnter(Collider other)
    {
        otherEnemy = other.gameObject;
    }
}
