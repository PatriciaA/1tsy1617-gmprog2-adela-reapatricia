﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour
{

    public GameObject objectEnemy;

    private Vector3 randomPos;
    private int maxEnemyCnt = 25;
    private int startEnemyCnt = 3;
    private float spawnRate = 3f;
    [SerializeField]
    private int currentEnemyCnt = 0;

    float timer = 0;

    void Start()
    {
        for (int i = 0; i < startEnemyCnt; i++)
        {
            float posX = Random.Range(-9.0f, 9.0f);
            float posY = Random.Range(-9.0f, 9.0f);

            randomPos = new Vector3(posX, posY, 0);

            GameObject objEnemy = (GameObject)Instantiate(objectEnemy, randomPos, Quaternion.identity);
            objEnemy.transform.SetParent(this.transform);
            currentEnemyCnt++;
        }
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > spawnRate)
        {
            SpawnEnemy();
            timer = 0;
        }
    }

    void SpawnEnemy()
    {
        if (currentEnemyCnt < maxEnemyCnt)
        {
            float posX = Random.Range(-9.0f, 9.0f);
            float posY = Random.Range(-9.0f, 9.0f);

            randomPos = new Vector3(posX, posY, 0);

            GameObject objEnemy = (GameObject)Instantiate(objectEnemy, randomPos, Quaternion.identity);
            objEnemy.transform.SetParent(this.transform);
            currentEnemyCnt++;
        }
    }
}
