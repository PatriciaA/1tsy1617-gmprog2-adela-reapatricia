﻿using UnityEngine;
using System.Collections;

public class EnemyEatFood : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
    }
}
