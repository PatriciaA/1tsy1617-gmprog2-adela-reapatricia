﻿using UnityEngine;
using System.Collections;

public class SpawnManager : MonoBehaviour {

    public GameObject objectFood;

    private Vector3 randomPos;
    private int maxFoodCnt = 100;
    private int startFoodCnt = 30;
    private float spawnRate = 1f;
    [SerializeField]
    private int currentFoodCnt = 0;

    float timer = 0;

    void Start()
    {
        for(int i = 0; i < startFoodCnt; i++)
        {
            float posX = Random.Range(-9.0f, 9.0f);
            float posY = Random.Range(-9.0f, 9.0f);

            randomPos = new Vector3(posX, posY, 0);

            GameObject objFood = (GameObject)Instantiate(objectFood, randomPos, Quaternion.identity);
            objFood.transform.SetParent(this.transform);
            currentFoodCnt++;
        }
    }

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > spawnRate)
        {
            SpawnFood();
            timer = 0;
        }
    }

    void SpawnFood()
    {
        if (currentFoodCnt < maxFoodCnt)
        {
            float posX = Random.Range(-9.0f, 9.0f);
            float posY = Random.Range(-9.0f, 9.0f);

            randomPos = new Vector3(posX, posY, 0);
            
            GameObject objFood = (GameObject)Instantiate(objectFood, randomPos, Quaternion.identity);
            objFood.transform.SetParent(this.transform);
            currentFoodCnt++;
        }
    }
}
